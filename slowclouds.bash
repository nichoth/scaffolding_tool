#!/bin/bash -e
set -o noclobber
set -o pipefail

# constants
# account="slowclouds"
prog_name=`basename $0`
account="nichoth" # for testing
base_repo="1206-base"

#vars
mode=""
template_repo=""
project_name=""
local_repo=0



# call this if arguments are bad
exit_usage() {
  echo -e "Usage: $prog_name [-l] new <onepage|wp> project_name"
  echo -e "Options:\n-l\tLocal. Create a local repo only, without a new Bitbucket repo."
  exit 1
}

# exit b/c of an error
# takes one argument - a message to print
function error_exit
{
  echo "$1" 1>&2
  exit 1
}

# validate our options
OPTIND=1
while getopts ":l" opt; do
  case $opt in 
    l ) local_repo=1;;
    * ) exit_usage;;
  esac
done
shift $((OPTIND-1))


# validate our arguments
case $# in
    3 ) [[ "$1" == "new" ]] || exit_usage
        [[ "$2" == "onepage" || "$2" == "wp" ]] || exit_usage;;
    * ) exit_usage;;
esac

# assign our vars
mode="$1"
template_repo="1206-$2"
project_name="$3"


# check that we can make the dir
if [ -d "$project_name" ]; then
  error_exit "Failed: directory $project_name already exists."
fi

# TODO: 
#   check if template_repo exists on BB
#   check if base repo exists on BB
#   make it not need a password every time



if [[ $mode == "new" && $local_repo == 0 ]]; then
  # get user name for CURL
  printf "BitBucket user name: "
  read username

  # create repo on bitbucket
  response=$(curl -X POST -u ${username} -H "Content-Type: application/json" \
  https://api.bitbucket.org/2.0/repositories/${account}/${project_name} \
  -d '{"scm": "git", "is_private": true, "has_issues": true, "language": "html/css"}')

  # check for errors
  if echo "$response" | grep -q "error"; then
    echo "failed"
    echo $response
    error_exit
  fi

fi



# clone template repo
git clone https://bitbucket.org/${account}/${template_repo}.git ${project_name}

cd $project_name || error_exit


# remove template history
rm -rf .git
git init
git add --all
git commit -m 'First commit.'


# add the base files as subtree with remote 'base'
git remote add -f base https://bitbucket.org/${account}/${base_repo}.git
git subtree add --prefix css/${base_repo} base master --squash


if [[ $local_repo == 0 ]]; then
  # push this to our new project's remote
  git push -u origin --all

  # add origin remote
  git remote add origin https://bitbucket.org/${account}/${project_name}.git
fi