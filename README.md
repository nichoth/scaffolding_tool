# Slowclouds git tool #

This command line tool manages git Slowclouds' git repositories.

### Use ###
There is curretly only one command. It clones a template repository onto your local machine, sets up the subtree relationship with the base files, and pushes all this to a new repository on Bitbucket. 
`slowclouds new [onepage|wp] <new_project_name>`
The second argument specifies whether the project uses the onepage or wordpress template.

### Installation ###

1. Download this file. `git clone https://nichoth@bitbucket.org/nichoth/new_tool.git`
2. Add it to your path `sudo mv slowclouds.bash /usr/local/slowclouds`